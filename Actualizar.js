alumno={
  "num_control" :["100","101","102","103"],
  "nombre":["Oscar","Ricardo","Francisco","Emanuel"],
  "apellidos":["Lopez Luis","Cayetano Herrera","Leon Olazo","Martienez Hernandez"]
}


grupo={
  "clave":"m1",
  "nombre":"isb",
  "alumnos":["100","101","102","103"],
  "materias":["mate1","espa1","ing1","his1","geo1","edu1","etic1"],
  "profesores":["profesor1","profesor2","profesor3","profesor4","profesor5","profesor6","profesor7"]
 
}

materia={
  "clave":["mate1","espa1","ing1","his1","geo1","edu1","etic1"],
  "nombre":["Calculo DIferencial","Español Universal","Ingles 1","Historia Universal 1","Geografia 1","Educacion Basica ","Etica"]
}

profesor={
  "clave":["profesor1","profesor2","profesor3","profesor4","profesor5","profesor6","profesor7"],
  "nombre":["Jorge","Ricardo","Marisol","Benito","Adriana","paco","Cesar"],
  "grado_academico":["Licenciatura","Maestria","Doctorado","Licenciatura","Ingenieria","Doctorado","Licenciatura"]
}


/* Actualizar alumno */

var   Actualizar_alumno= function(n_ctrl,nom,ape,obj_alumno){
    var estado=false;
    var mensaje="";
    for(var i=0;i<alumno["num_control"].length;i++){
        if(n_ctrl===obj_alumno["num_control"][i]){
            estado=true;
          var actual=i;
        }
    }
    
    if(estado === true){
        var pos = actual;
        obj_alumno["num_control"][pos]=n_ctrl;
        obj_alumno["nombre"][pos]=nom;
		 obj_alumno["apellidos"][pos]=ape;
        mensaje="Se Actualizo al alumno(a) "+nom+" "+ape+" con el numero de control :"+n_ctrl;
    
    console.log(actual);
    }
    else{
        mensaje="El numero de Control "+n_ctrl+" no Existe...";
    }
    return mensaje;
}

Actualizar_alumno("100","juanito","luis",alumno);





/* Actualizar profesor */

var   Actualizar_profesor= function(_clave,_nom,_grado,obj_profesor){
    var estado=false;
    var mensaje="";
    for(var i=0;i<profesor["clave"].length;i++){
        if(_clave===obj_profesor["clave"][i]){
            estado=true;
          var actual=i;
        }
    }
    
    if(estado === true){
        var pos = actual;
        obj_profesor["clave"][pos]=_clave;
        obj_profesor["nombre"][pos]=_nom;
		 obj_profesor["grado_academico"][pos]=_grado;
        mensaje="Se Actualizo al profesor(a) "+_nom+" "+ _grado+" con  la clave :"+_clave;
    
    console.log(actual);
    }
    else{
        mensaje="La clave "+_clave+" no Existe...";
    }
    return mensaje;
}

Actualizar_profesor("profesor1","juanito","Ingeniero",profesor);




/* Actualizar Materia*/

var   Actualizar_materia= function(_clave,_nom,obj_materia){
    var estado=false;
    var mensaje="";
    for(var i=0;i<materia["clave"].length;i++){
        if(_clave===obj_materia["clave"][i]){
            estado=true;
          var actual=i;
        }
    }
     if(estado === true){
        var pos = actual;
        obj_materia["clave"][pos]=_clave;
        obj_materia["nombre"][pos]=_nom;
		mensaje="Se Actualizo la materia "+_nom+" con  la clave :"+_clave;
    
    console.log(actual);
    }
    else{
        mensaje="La clave "+_clave+" no Existe...";
    }
    return mensaje;
}

Actualizar_materia("mate1","Ecuaciones",materia);




/*  Agregar Alumno */

  var  agregar_alumno= function (n_ctrl,nom,ape,obj_alumno){
    var estado=false;
    var text="";
    for(var i=0;i<alumno["num_control"].length;i++){
        if(n_ctrl==obj_alumno["num_control"][i]){
            estado=true;
        }
    }
    
    if(estado === false){
        var pos = (obj_alumno["num_control"].length);
        obj_alumno["num_control"][pos]=n_ctrl;
        obj_alumno["nombre"][pos]=nom;
		 obj_alumno["apellidos"][pos]=ape;
        text="Se agrego al alumno(a) "+nom+" "+ape+" con el numero de control "+n_ctrl;
    }
    else{
        text="El numero de COntrol "+n_ctrl+" ya existe porfavor introduzca otro...";
    }
    return text;
}

agregar_alumno("11","juanito","mendez",alumno);

/*     Agregar Materia   */



var agregar_materia= function (n_clave,nom,obj_materia){
    var estado=false;
    var text="";
    for(var i=0;i<materia["clave"].length;i++){
        if(n_clave==obj_materia["clave"][i]){
            estado=true;
        }
    }
    
    if(estado === false){
        var pos = (obj_materia["clave"].length
                  
                  );
        obj_materia["clave"][pos]=n_clave;
        obj_materia["nombre"][pos]=nom;
        text="Se agrego la materia "+nom+" con la clave "+n_clave;
    }
    else{
        text="La clave "+n_clave+" ya existe porfavor introduzca otro...";
    }
    return text;
}


agregar_materia("we","españollll",materia)
visualizar_Materia (materia);


/* Agregar Profesor   */


 var agregar_profesor = function (n_clave,nom,ape,grado,obj_profesor){
    var estado=false;
    var text="";
    for(var i=0;i<profesor["clave"].length;i++){
        if(n_clave==obj_profesor["clave"][i]){
            estado=true;
        }
    }
    
    if(estado === false){
        var pos = (obj_profesor["clave"].length+1);
        obj_profesor["clave"][pos]=n_clave;
        obj_profesor["nombre"][pos]=nom;
		obj_profesor["apellidos"][pos]=ape;
		obj_profesor["grado_academico"][pos]=grado;
		
        text="Se agrego la materia "+nom+" con la clave "+n_clave;
    }
    else{
        text="La clave "+n_clave+" ya existe porfavor introduzca otro...";
    }
    return text;
}

agregar_profesor("we","jose","hernandez","licenciado",profesor);




/* Asignar Profesor al Grupo */

var agregar_Profesor_al_grupo= function (obj_grupo,obj_profesor,_clave_profesor,obj_materia,_clave_materia){
    var estado=false;
  var estado2=false;
    var text="";
   var text2="";
  var text3="";
    for(var i=0;i<obj_profesor["clave"].length;i++){
        if(_clave_profesor==obj_profesor["clave"][i] ){
            estado=true;
            var actual=i;
          
          
          for(var i=0;i<obj_profesor["clave"].length;i++){
        if(_clave_materia==obj_materia["nombre"][i] ){
            estado2=true;
   
        }
      }
          
    
          
          
        }
  
    }
    
    if(estado === true && estado2 === true){
        var pos = (obj_grupo["profesores"].length);
      obj_grupo["materias"][pos]=_clave_materia;
      obj_grupo["profesores"][pos]=_clave_profesor;
      
      text="Se agrego El profesor "+obj_profesor["nombre"][i]+" con la materia "+mate;
    }
  else {
    
    if(estado === false){
      text2="EL profesor no Existe";
      
     
    }
    if(estado2 === false){   text3="La materia no Existe";} 
     return text3, text2; 
    
    
    
    }
    return text;
}


 agregar_Profesor_al_grupo(grupo,profesor,"profsor1",materia,"historia")

 visualizar_Grupo(grupo)